package br.com.pedronsouza.skydigital.domain.data.datasource

import io.reactivex.functions.Consumer

interface BaseUseCase<T> {
    fun execute(consumer: (T) -> Unit)
    fun dispose()
}