package br.com.pedronsouza.skydigital.domain.data.runners

import io.reactivex.Scheduler

interface PostExecutionThread {
    fun scheduler() : Scheduler
}