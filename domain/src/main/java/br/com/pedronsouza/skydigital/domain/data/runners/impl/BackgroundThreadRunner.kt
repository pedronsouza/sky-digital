package br.com.pedronsouza.skydigital.domain.data.runners.impl

import br.com.pedronsouza.skydigital.domain.data.runners.BackgroundExecutionThread
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.RejectedExecutionHandler
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class BackgroundThreadRunner : BackgroundExecutionThread {
    constructor() {
        this.threadPoolExecutor = ThreadPoolExecutor(corePoolSize, maxPoolSize,
            keepAliveTime, timeUnit, workQueue)
    }

    private val corePoolSize = 3
    private val maxPoolSize = 5
    private val keepAliveTime = 120L
    private val timeUnit = TimeUnit.SECONDS
    private val workQueue = LinkedBlockingQueue<Runnable>()
    private val rejectedExecutionHandler = RejectedExecutionHandler { _, _ -> threadPoolExecutor!!.shutdown() }
    private var threadPoolExecutor: ThreadPoolExecutor? = null


    override fun execute(interactor: Runnable?) {
        if (interactor == null) {
            throw IllegalArgumentException("Interactor must not be null")
        }
        threadPoolExecutor?.rejectedExecutionHandler = rejectedExecutionHandler
        threadPoolExecutor?.submit(interactor)
    }

}