package br.com.pedronsouza.skydigital.domain.data.runners

import java.util.concurrent.Executor

interface BackgroundExecutionThread : Executor {

}