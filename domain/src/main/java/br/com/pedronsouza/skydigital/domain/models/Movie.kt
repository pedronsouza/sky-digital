package br.com.pedronsouza.skydigital.domain.models

import com.google.gson.annotations.SerializedName

data class Movie(
    val id : String,
    val title : String?,
    val overview: String?,
    val duration: String?,
    @SerializedName("release_year") val releaseYear: String?,
    @SerializedName("cover_url") val coverImageUrl: String?,
    @SerializedName("backdrops_url") val backgroundsUrl: List<String>
)
