package br.com.pedronsouza.skydigital.domain.data.datasource

import br.com.pedronsouza.skydigital.domain.models.Movie
import io.reactivex.Observable

interface MoviesDataSource {
    fun all() : Observable<List<Movie>>
}