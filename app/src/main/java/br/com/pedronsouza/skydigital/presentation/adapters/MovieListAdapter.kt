package br.com.pedronsouza.skydigital.presentation.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.pedronsouza.skydigital.R
import br.com.pedronsouza.skydigital.presentation.viewmodels.MovieListItemViewModel
import br.com.pedronsouza.skydigital.support.CircleProgressBarDrawable
import com.facebook.drawee.view.SimpleDraweeView

class MovieListAdapter(private val items : List<MovieListItemViewModel>, private val callback: (MovieListItemViewModel) -> Unit ) : RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>() {
    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_movie_list_item, parent, false)
        return MovieViewHolder(v)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) =
        holder.bindWithModel(items[position])

    inner class MovieViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        private val movieCover by lazy { itemView.findViewById<SimpleDraweeView>(R.id.movieCover) }
        private val movieTitle by lazy { itemView.findViewById<TextView>(R.id.movieTitle) }

        fun bindWithModel(item : MovieListItemViewModel) {
            movieTitle.text = item.title
            movieCover.hierarchy.setProgressBarImage(CircleProgressBarDrawable())
            movieCover.setImageURI(Uri.parse(item.thumbUrl))
            itemView.setOnClickListener { callback(item) }
        }
    }
}