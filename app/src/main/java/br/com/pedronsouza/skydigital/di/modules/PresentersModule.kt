package br.com.pedronsouza.skydigital.di.modules

import br.com.pedronsouza.skydigital.data.usecases.FetchAllPrimeVideosUseCase
import br.com.pedronsouza.skydigital.presentation.presenters.MoviesListPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresentersModule {
    @Provides
    @Singleton
    fun provideMovieListPresenter(fetchAllPrimeVideosUseCase: FetchAllPrimeVideosUseCase) : MoviesListPresenter {
        return MoviesListPresenter(fetchAllPrimeVideosUseCase)
    }
}