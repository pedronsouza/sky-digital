package br.com.pedronsouza.skydigital.presentation.controllers.activities

import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import br.com.pedronsouza.skydigital.App
import br.com.pedronsouza.skydigital.R
import br.com.pedronsouza.skydigital.presentation.adapters.FragmentStatePagerAdapter
import br.com.pedronsouza.skydigital.presentation.controllers.fragments.ImageCoverFragment
import br.com.pedronsouza.skydigital.presentation.viewmodels.MovieListItemViewModel
import kotlinx.android.synthetic.main.activity_details.*
import java.util.*

class DetailsActivity : AppCompatActivity() {
    companion object {
        const val movieDetailKey = "movieDetailKey"
    }

    private val movie by lazy { intent.extras.getParcelable(movieDetailKey) as MovieListItemViewModel }
    private val timer = Timer()
    private val handler = Handler()
    private val task = object: TimerTask() {
        override fun run() {
            handler.post(Update)
        }
    }

    private val Update = Runnable {
        if (coverImagesPager.currentItem == (movie.coverImages.size - 1)) {
            coverImagesPager.setCurrentItem(0, true)
        } else {
            coverImagesPager.setCurrentItem((coverImagesPager.currentItem + 1), true)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        setContentView(R.layout.activity_details)

        onViewCreated()
    }

    fun onViewCreated() {
        setSupportActionBar(detailsToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = movie.title
        val whiteColor = resources.getColor(R.color.colorAccent)
        detailsToolbar?.setTitleTextColor(whiteColor)
        detailsToolbar?.navigationIcon?.setColorFilter(whiteColor, PorterDuff.Mode.SRC_ATOP)
        timer.scheduleAtFixedRate(task, 1000L, 2000L)

        movieDescription.text = movie.description

        val items = movie.coverImages.map { i -> ImageCoverFragment.newInstance(i) }
        coverImagesPager.adapter = FragmentStatePagerAdapter(items, supportFragmentManager)
    }
}