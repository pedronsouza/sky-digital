package br.com.pedronsouza.skydigital.presentation.controllers.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import br.com.pedronsouza.skydigital.App
import br.com.pedronsouza.skydigital.R
import br.com.pedronsouza.skydigital.presentation.adapters.MovieListAdapter
import br.com.pedronsouza.skydigital.presentation.presenters.MoviesListPresenter
import br.com.pedronsouza.skydigital.presentation.viewmodels.MovieListItemViewModel
import br.com.pedronsouza.skydigital.presentation.views.MoviesListView
import kotlinx.android.synthetic.main.activity_movies.*
import javax.inject.Inject

class MoviesActivity : AppCompatActivity(), MoviesListView {
    @Inject lateinit var presenter : MoviesListPresenter

    private val onMovieItemListTapped : (MovieListItemViewModel) -> Unit = { movie ->
        val i = Intent(this@MoviesActivity, DetailsActivity::class.java)
        i.putExtra(DetailsActivity.movieDetailKey, movie)
        startActivity(i)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        presenter.onViewCreate(this)
        setContentView(R.layout.activity_movies)
        refreshLayout.isRefreshing = true
        refreshLayout.setOnRefreshListener { presenter.refresh() }
    }

    override fun handleMovieListFromServer(movies: List<MovieListItemViewModel>) {
        movieList.adapter = MovieListAdapter(movies, onMovieItemListTapped)
        refreshLayout.isRefreshing = false
    }

    override fun handleMovieListFetchError(e: Throwable) {
        refreshLayout.isRefreshing = false
        AlertDialog.Builder(this)
            .setTitle(R.string.alert_dialog_title)
            .setMessage(R.string.alert_dialog_message)
            .setNegativeButton(R.string.alert_dialog_cancel, null)
            .setPositiveButton(R.string.alert_dialog_try_again) { _, _ ->
                refreshLayout.isRefreshing = true
                presenter.refresh()
            }
            .show()
    }

    override fun onStop() {
        super.onStop()
        presenter.dispose()
    }
}
