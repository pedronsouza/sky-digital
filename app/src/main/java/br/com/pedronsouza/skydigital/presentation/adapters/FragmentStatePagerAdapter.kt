package br.com.pedronsouza.skydigital.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class FragmentStatePagerAdapter(val items : List<Fragment>, val fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment =
        items[position]

    override fun getCount(): Int =
        items.size

}