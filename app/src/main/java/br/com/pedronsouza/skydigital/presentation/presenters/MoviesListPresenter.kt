package br.com.pedronsouza.skydigital.presentation.presenters

import br.com.pedronsouza.skydigital.data.usecases.FetchAllPrimeVideosUseCase
import br.com.pedronsouza.skydigital.domain.models.Movie
import br.com.pedronsouza.skydigital.presentation.viewmodels.MovieListItemViewModel
import br.com.pedronsouza.skydigital.presentation.views.MoviesListView
import javax.inject.Inject

class MoviesListPresenter : BasePresenter<MoviesListView> {
    private var fetchAllPrimeVideosUseCase: FetchAllPrimeVideosUseCase

    @Inject constructor(fetchAllPrimeVideosUseCase : FetchAllPrimeVideosUseCase) {
        this.fetchAllPrimeVideosUseCase = fetchAllPrimeVideosUseCase
        this.fetchAllPrimeVideosUseCase.doOnError = onMoviesFetchedError
    }

    private val onMoviesFetched : (List<Movie>) -> Unit =  {movies ->
        val items = movies.map { m -> MovieListItemViewModel(m.id, m.title!!, m.overview!!, m.backgroundsUrl[0], m.backgroundsUrl) }
        this.view.handleMovieListFromServer(items)
    }

    private val onMoviesFetchedError : (Throwable) -> Unit = {
        this.view.handleMovieListFetchError(it)
    }

    fun refresh() {
        fetchAllPrimeVideosUseCase.execute(onMoviesFetched)
    }

    override fun onViewCreate(view: MoviesListView) {
        this.view = view

        fetchAllPrimeVideosUseCase.execute(onMoviesFetched)
    }

    override fun dispose() {
        fetchAllPrimeVideosUseCase.dispose()
    }
}