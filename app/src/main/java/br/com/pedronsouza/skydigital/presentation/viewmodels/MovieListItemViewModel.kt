package br.com.pedronsouza.skydigital.presentation.viewmodels

import android.os.Parcel
import android.os.Parcelable

data class MovieListItemViewModel(val id : String,
                                  val title : String,
                                  val description : String,
                                  val thumbUrl: String,
                                  val coverImages : List<String>) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createStringArray().toList()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(title)
        dest?.writeString(description)
        dest?.writeString(thumbUrl)
        dest?.writeStringArray(coverImages.toTypedArray())
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<MovieListItemViewModel> {
        override fun createFromParcel(parcel: Parcel): MovieListItemViewModel {
            return MovieListItemViewModel(parcel)
        }

        override fun newArray(size: Int): Array<MovieListItemViewModel?> {
            return arrayOfNulls(size)
        }
    }
}