package br.com.pedronsouza.skydigital.di.modules

import br.com.pedronsouza.skydigital.BuildConfig
import br.com.pedronsouza.skydigital.data.net.AppHttpClient
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetModule {
    @Provides
    fun provideAppHttpClient(retrofit: Retrofit) : AppHttpClient {
        return AppHttpClient(retrofit)
    }

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://sky-exercise.herokuapp.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    fun provideOkHttp() : OkHttpClient {
        val level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else  HttpLoggingInterceptor.Level.NONE
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = level
        return OkHttpClient
            .Builder()
            .addInterceptor(interceptor)
            .build()
    }
}