package br.com.pedronsouza.skydigital.di.modules

import br.com.pedronsouza.skydigital.data.datasource.RemoteMovieDataSource
import br.com.pedronsouza.skydigital.data.datasource.core.runners.MainThreadRunner
import br.com.pedronsouza.skydigital.data.net.AppHttpClient
import br.com.pedronsouza.skydigital.data.usecases.FetchAllPrimeVideosUseCase
import br.com.pedronsouza.skydigital.domain.data.datasource.MoviesDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [NetModule::class])
class DataModule {

    @Provides
    @Singleton
    fun provideMovieDataSource(httpClient: AppHttpClient) : MoviesDataSource {
        return RemoteMovieDataSource(httpClient)
    }

    @Provides
    fun provideFetchAllPrimeVideosUseCase(threadExecutor: MainThreadRunner, dataSource: MoviesDataSource) : FetchAllPrimeVideosUseCase {
        return FetchAllPrimeVideosUseCase(threadExecutor, dataSource)
    }
}