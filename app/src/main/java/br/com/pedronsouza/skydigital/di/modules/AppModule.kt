package br.com.pedronsouza.skydigital.di.modules

import br.com.pedronsouza.skydigital.data.datasource.core.runners.MainThreadRunner
import dagger.Module
import dagger.Provides

@Module(includes = [DataModule::class, PresentersModule::class])
class AppModule {
    @Provides
    fun provideMainThreadRunner() : MainThreadRunner {
        return MainThreadRunner()
    }
}