package br.com.pedronsouza.skydigital.presentation.views

import br.com.pedronsouza.skydigital.presentation.viewmodels.MovieListItemViewModel

interface MoviesListView : BaseView {
    fun handleMovieListFromServer(movies : List<MovieListItemViewModel>)
    fun handleMovieListFetchError(e : Throwable)
}