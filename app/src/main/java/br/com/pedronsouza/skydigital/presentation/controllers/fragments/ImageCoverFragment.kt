package br.com.pedronsouza.skydigital.presentation.controllers.fragments

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.pedronsouza.skydigital.R
import com.facebook.drawee.view.SimpleDraweeView


class ImageCoverFragment : Fragment() {
    companion object {

        val IMAGE_COVER_MOVIE_KEY = "IMAGE_COVER_MOVIE"

        fun newInstance(coverImage : String) : ImageCoverFragment {
            val frag = ImageCoverFragment()
            frag.arguments = Bundle()
            frag.arguments?.putString(IMAGE_COVER_MOVIE_KEY, coverImage)
            return frag
        }
    }

    private val coverImage by lazy { arguments?.getString(IMAGE_COVER_MOVIE_KEY, null)}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_cover, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val draweeView = view.findViewById<SimpleDraweeView>(R.id.imageCover)
        draweeView.setImageURI(Uri.parse(coverImage))
    }
}