package br.com.pedronsouza.skydigital

import android.app.Application
import br.com.pedronsouza.skydigital.di.AppComponent
import br.com.pedronsouza.skydigital.di.DaggerAppComponent
import br.com.pedronsouza.skydigital.di.modules.AppModule
import com.facebook.drawee.backends.pipeline.Fresco

class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule())
            .build()
        Fresco.initialize(this);
    }
}