package br.com.pedronsouza.skydigital.presentation.presenters

import br.com.pedronsouza.skydigital.presentation.views.BaseView
import br.com.pedronsouza.skydigital.presentation.views.MoviesListView

abstract class BasePresenter<T : BaseView> {
    lateinit var view : T
    abstract fun onViewCreate(view : T)
    abstract fun dispose()
}