package br.com.pedronsouza.skydigital.di

import br.com.pedronsouza.skydigital.data.net.AppHttpClient
import br.com.pedronsouza.skydigital.data.usecases.FetchAllPrimeVideosUseCase
import br.com.pedronsouza.skydigital.di.modules.AppModule
import br.com.pedronsouza.skydigital.di.modules.DataModule
import br.com.pedronsouza.skydigital.di.modules.NetModule
import br.com.pedronsouza.skydigital.di.modules.PresentersModule
import br.com.pedronsouza.skydigital.domain.data.datasource.MoviesDataSource
import br.com.pedronsouza.skydigital.presentation.controllers.activities.DetailsActivity
import br.com.pedronsouza.skydigital.presentation.controllers.activities.MoviesActivity
import br.com.pedronsouza.skydigital.presentation.presenters.MoviesListPresenter
import dagger.Component
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DataModule::class, PresentersModule::class, NetModule::class])
interface AppComponent {
    fun movieListPresenter() : MoviesListPresenter
    fun appHttpClient() : AppHttpClient
    fun fetchAllPrimeVideosUseCase() : FetchAllPrimeVideosUseCase
    fun moviesDataSource() : MoviesDataSource
    fun retrofit() : Retrofit
    fun okHttpClient() : OkHttpClient

    fun inject(view : MoviesActivity)
    fun inject(view: DetailsActivity)
}