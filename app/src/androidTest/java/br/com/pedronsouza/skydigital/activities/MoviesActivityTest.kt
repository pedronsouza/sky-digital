package br.com.pedronsouza.skydigital.activities

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import br.com.pedronsouza.skydigital.R
import br.com.pedronsouza.skydigital.TestHelper
import br.com.pedronsouza.skydigital.presentation.controllers.activities.DetailsActivity
import br.com.pedronsouza.skydigital.presentation.controllers.activities.MoviesActivity
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class MoviesActivityTest {
    @get:Rule
    val activityRule = ActivityTestRule(MoviesActivity::class.java)

    lateinit var server : MockWebServer

    @Before fun setUp() {
        server = MockWebServer()
        server.start()
        Intents.init()
    }

    @After fun tearDown() {
        Intents.release()
    }

    @Test fun shouldShowListOfItems() {
        val i = Intent()
        activityRule.launchActivity(i)
        onView(withId(R.id.movieList)).check(matches(isDisplayed()))
    }

    @Test fun shouldIncludeItems() {
        val i = Intent()
        activityRule.launchActivity(i)
        val json = TestHelper.getJsonFixtures("fixtures_movies.json")
        val match = json?.getJSONObject(0)?.getString("title")
        server.enqueue(MockResponse().setBody(json?.toString()))

        activityRule.launchActivity(i)
        onView(withId(R.id.movieList)).check(matches(isDisplayed()))
        onView(withText(match)).check(matches(isDisplayed()))
    }

    @Test fun shouldRedirectToDetailScreen() {
        val i = Intent()
        activityRule.launchActivity(i)
        val json = TestHelper.getJsonFixtures("fixtures_movies.json")
        val match = json?.getJSONObject(0)?.getString("title")
        server.enqueue(MockResponse().setBody(json?.toString()))
        onView(withText(match)).perform(click())
        intended(hasComponent(DetailsActivity::class.java.name))
    }
}