package br.com.pedronsouza.skydigital

import androidx.test.InstrumentationRegistry
import org.json.JSONArray
import org.json.JSONObject

class TestHelper {
    companion object {
        fun getJsonFixtures(jsonFileName : String) : JSONArray? {
            val input = InstrumentationRegistry.getContext().resources.assets.open(jsonFileName)
            val s = java.util.Scanner(input).useDelimiter("\\A")
            val rawJson = if (s.hasNext()) s.next() else ""
            if (rawJson.isNotEmpty()) {
                return JSONArray(rawJson)
            }

            return null
        }
    }
}