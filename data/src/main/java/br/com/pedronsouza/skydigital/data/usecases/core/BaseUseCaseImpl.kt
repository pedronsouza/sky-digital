package br.com.pedronsouza.skydigital.data.usecases.core

import br.com.pedronsouza.skydigital.data.datasource.core.runners.MainThreadRunner
import br.com.pedronsouza.skydigital.domain.data.datasource.BaseUseCase
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

abstract class BaseUseCaseImpl<T> : BaseUseCase<T> {
    private var mainThreadExecutor : MainThreadRunner

    constructor(mainThreadExecutor : MainThreadRunner) {
        this.mainThreadExecutor = mainThreadExecutor
    }

    private var subscription : Disposable? = null
    abstract fun observableFor() : Observable<T>

    override fun execute(consumer: (T) -> Unit) {
        val observer = observableFor()

        subscription = observer
                .subscribeOn(Schedulers.computation())
            .observeOn(mainThreadExecutor.scheduler())
            .subscribe(consumer)
    }

    override fun dispose() {
        this.subscription?.dispose()
    }
}