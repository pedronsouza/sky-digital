package br.com.pedronsouza.skydigital.data.datasource.core

import android.os.Parcelable
import br.com.pedronsouza.skydigital.data.net.AppHttpClient

abstract class RemoteBaseDataSource(protected val client : AppHttpClient)