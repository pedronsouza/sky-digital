package br.com.pedronsouza.skydigital.data.net.services

import br.com.pedronsouza.skydigital.domain.models.Movie
import io.reactivex.Observable
import retrofit2.http.GET

interface MoviesService {
    @GET("movies")
    fun fetchAll() : Observable<List<Movie>>
}