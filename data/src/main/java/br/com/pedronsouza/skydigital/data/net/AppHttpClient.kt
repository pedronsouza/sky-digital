package br.com.pedronsouza.skydigital.data.net

import br.com.pedronsouza.skydigital.data.net.services.MoviesService
import retrofit2.Retrofit

class AppHttpClient(private val retrofit: Retrofit) {
    val moviesServices by lazy { retrofit.create(MoviesService::class.java) }
}