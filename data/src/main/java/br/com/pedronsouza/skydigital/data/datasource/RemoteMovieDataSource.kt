package br.com.pedronsouza.skydigital.data.datasource

import br.com.pedronsouza.skydigital.data.datasource.core.RemoteBaseDataSource
import br.com.pedronsouza.skydigital.data.net.AppHttpClient
import br.com.pedronsouza.skydigital.domain.data.datasource.MoviesDataSource
import br.com.pedronsouza.skydigital.domain.models.Movie
import io.reactivex.Observable
import javax.inject.Inject

class RemoteMovieDataSource : RemoteBaseDataSource, MoviesDataSource {
    override fun all() : Observable<List<Movie>> =
        this.client.moviesServices.fetchAll()

    @Inject constructor(client : AppHttpClient) : super (client)
}