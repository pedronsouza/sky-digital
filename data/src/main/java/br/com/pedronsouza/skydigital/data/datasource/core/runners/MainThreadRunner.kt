package br.com.pedronsouza.skydigital.data.datasource.core.runners

import br.com.pedronsouza.skydigital.domain.data.runners.PostExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

class MainThreadRunner : PostExecutionThread {
    override fun scheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}