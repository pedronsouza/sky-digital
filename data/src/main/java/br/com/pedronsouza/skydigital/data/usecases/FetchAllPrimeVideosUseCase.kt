package br.com.pedronsouza.skydigital.data.usecases

import br.com.pedronsouza.skydigital.data.datasource.core.runners.MainThreadRunner
import br.com.pedronsouza.skydigital.data.usecases.core.BaseUseCaseImpl
import br.com.pedronsouza.skydigital.domain.data.datasource.MoviesDataSource
import br.com.pedronsouza.skydigital.domain.models.Movie
import io.reactivex.Observable
import javax.inject.Inject

class FetchAllPrimeVideosUseCase : BaseUseCaseImpl<List<Movie>> {
    private var dataSource: MoviesDataSource
    lateinit var doOnError : (Throwable) -> Unit

    @Inject constructor(threadExecutor : MainThreadRunner, dataSource: MoviesDataSource) : super(threadExecutor) {
        this.dataSource = dataSource
    }

    override fun observableFor(): Observable<List<Movie>> =
        this.dataSource.all().doOnError(doOnError)
}